/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ASUS
 */
public class SanjiAdapter implements AnimeHero{
    private Sanji sanji;
    public SanjiAdapter(Sanji sanji){
        this.sanji = sanji;
    }
    @Override
    public String attack(){
        return this.sanji.kick();
    }
    
}
